## Summary
NuGet packaging for PostgreSQL Server binaries.

## Performing an Update
### Windows
1. Download the latest version from [https://www.enterprisedb.com/download-postgresql-binaries](https://www.enterprisedb.com/download-postgresql-binaries).
1. Clean the `pgsql/<runtime>/` directory in this repository by deleting all subdirectories & files within it.
1. Extract the following directories from the downloaded archive to the local `pgsql/win-x64/` directory:
    * `bin/`
    * `lib/`
    * `share/`
1. Extract `doc/postgresql/html/legalnotice.html` to `pgsql/`

### Linux 
1. In the `.gitlab-ci.yml`, update the build job by changing the download path to the `curl` command to the latest source. You may also need to change other commands to match the new verzion number.

### Final Steps
1. Commit updated postgres files in one self-contained commit.
1. Update the version in the `.nuspec` file in a separate commit.
