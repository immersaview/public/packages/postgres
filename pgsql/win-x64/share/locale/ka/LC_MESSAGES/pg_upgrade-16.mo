Þ         ¬  o  <      ø     ù     ý  
     Ï   #  h   ó  \   \  Ý   ¹  &        ¾  "   Õ  R   ø  ]   K     ©  ¾   Ê  é     C  s     ·     Ê     Ý  N   }  D   Ì  :         L   ;   Õ   U   !  J   g!  E   ²!  A   ø!  G   :"  A   "  K   Ä"  ;   #  S   L#  M    #  J   î#  E   9$  G   $  O   Ç$  @   %  ¢   X%     û%     &     &     ,&     B&     Z&     v&      &     ·&     Õ&  $   ï&  (   '     ='  &   Z'     '     '     º'     Î'  #   ê'     (     )(     G(  %   d(  $   (     ¯(     Ç(  !   Ì(  "   î(     )     ))     ,)     @)     P)  )   j)  -   )  |  Â)  %   ?+  A   e+  N   §+     ö+  %   ,  *   6,  5   a,     ,     ¶,  <   Ð,  +   -  *   9-  /   d-  2   -  +   Ç-  /   ó-  "   #.  +   F.  +   r.  &   .  :   Å.      /  .   /  +   M/     y/  I   /  Q   ß/     10     N0  ß   j0  !   J1     l1  %   1     ²1     ¿1  6   Ú1     2  6   #2  6   Z2  $   2     ¶2     Ç2  J   ã2  >   .3  O   m3  O   ½3  J   4  8   X4     4  	   &5  ;   05  a   l5  [   Î5     *6  -   A6  +   o6     6  D   ³6     ø6  5   7  +   G7  &   s7      7  4   »7  5   ð7  "   &8  *   I8     t8  1   8  3   Â8  ;   ö8  -   29  }   `9  1   Þ9  3   :  ;   D:  -   :  }   ®:  h   ,;  h   ;  "   þ;  C   !<  7   e<  L   <  &   ê<     =  L   =  =   f=  s   ¤=    >  0  ?  Ü   Î@  "  «A  Ü   ÎB  #  «C    ÏD  ;  åE    !G    <H  G  LI    J     -L    .M  K   2N     ~N     N  %   ²N  C   ØN  *   O  "   GO     jO     O     O  #   ¥O  1   ÉO  =   ûO  C   9P  C   }P  #   ÁP     åP     Q  #   Q  "   »Q     ÞQ  %   þQ  '   $R  '   LR  )   tR  .   R  +   ÍR     ùR  (   S     BS      _S  $   S  !   ¥S  )   ÇS  0   ñS  &   "T  ,   IT  $   vT  *   T  *   ÆT  B   ñT  7   4U  D   lU  B   ±U  7   ôU  D   ,V  B   qV  B   ´V  B   ÷V  C   :W  E   ~W  A   ÄW     X     X  +   X     FX     ^X  >   vX     µX     ÊX     æX  5   ÿX     5Y     <Y  A   ?Y  F   Y  H   ÈY  {   Z  B   Z  ?   ÐZ  O   [  P   `[  Q   ±[  N   \  U   R\     ¨\     Ä\  <   Ý\  8   ]  .   S]     ]  >   ]  >   Ï]  H   ^  *   W^     ^     ^  (   ±^  '   Ú^  U   _     X_  /   l_  2   _     Ï_  Å  ×_     a  A   ¡a     ãa  ¦  ûa  |   ¢c  F  d  @  fe  T   §g  P   üg  R   Mh     h  M  §i  :  õj  Ç  0m    øn  ¥  ~q  *   $u  *   Ou     zu  ª   v  Ð   Åv  u   w    x     'y  £   ¨y  §   Lz     ôz  Â   {     W|     à|  ¢   u}     ~  °   ~  «   J  §   ö       ¦   <     ã  x   s  ¢   ì  %     5   µ      ë  S     e   `  a   Æ  l   (  J     Y   à  M   :  q     u   ú  i   p  t   Ú  l   O  O   ¼  9     d   F     «  /   ,  7   \  ;     J   Ð  M     J   i     ´  C   ¹  m   ý  0   k       <     %   Ü  2        5  p   Ì  J  =  g     ¬   ð  )    S   Ç       »   ¡  n   ]  b   Ì  J   /  ½   z  t   8  s   ­     !     ¿  |   C  ¡   À  e   b  r   È     ;  k   Ï  ß   ;  A     ³   ]  ¯     {   Á  ¿   =  Ö   ý  Y   Ô   x   .¡    §¡  j   G¤  `   ²¤  v   ¥     ¥  E   ¥     á¥  3   ~¦     ²¦     E§  g   Ø§  3   @¨  u   t¨    ê¨  À   
ª  Ê   Ëª  Ê   «  ý   a¬  Ê   _­  F  *®      q¯     ¯  Õ   °  Í   ï°  A   ½±  g   ÿ±  |   g²  \   ä²  ³   A³  ;   õ³  y   1´  }   «´  b   )µ  j   µ  Í   ÷µ  ¯   Å¶  m   u·     ã·  ~   y¸     ø¸     ¹  °   º  o   Àº  t  0»     ¥¼     .½  ³   ¿½  o   s¾  u  ã¾  Æ   YÀ  à    Á  A   Â  ¡   CÂ     åÂ  ñ   rÃ  [   dÄ      ÀÄ  Ý   áÄ  ¨   ¿Å    hÆ  =  Ç  j  ÄË  ¼  /Ï  X  ìÑ  N  EÕ  ý  ×  ý  Ú  w  Ý  ;  á    Dä  &  Sç  Ç  zê  ú  Bî  ã  =ñ  Ë   !ô  =   íô  =   +õ  i   iõ     Óõ     qö     ôö  9   z÷  A   ´÷  6   ö÷  S   -ø  |   ø  Ã   þø  °   Âù  ³   sú  S   'û  G   {û  Ø  Ãû  j   ý  g   þ  N   oþ  ~   ¾þ  f   =ÿ     ¤ÿ  }   (     ¦  [   ( a    ^   æ G   E `    ]   î e   L y   ² ~   , C   «    ï `   { ª   Ü             ¯    ¯   É    y	 ¬   ÿ	 ­   ¬
 ²   Z ²    ¬   À È   m    6    É    é v    N   y N   È °    '   È y   ð i   j ¥   Ô !   z     Ç   © °   q ¹   "   Ü    ô «    Ñ   9 Ú    ë   æ ß   Ò å   ² y    i    º   |    7    Ó C   d ª   ¨ ª   S    þ *     6   ¸  4   ï  r   $!    ! Ü   '" ,   # ¢   1# ³   Ô# !   $    ß   õ   Ç   c   \          ²                        -   d   `   ¨   ®       ¿   @      ;   %   Æ   §   ¤   ·   o       ó   Ó     Ý            	       æ       W      1   ¥       ô   
  {   2          f              Í   «   Ú      °   ù       ­   í           "       p       à       Q              K   ú   7   Ü          ¶   Õ          Þ              ^      ö   T                 5         È      ¬   H             Å       Ï   Î   Ö   ¸   Ò   a          ü   3   P   ò       l       ~   m   g       8      I      Ã               )   Y   *       Ø   Ä      A   }   è   ¹   ¡     b   ,       â      E   Ì   $   »   ë          ÿ   J   e   À   å          L         z   r   C       ½   t         ª            ç           s       é   B      R   w   V          v   !          þ      4   û       q   Á   £   ÷   Ô           h   6                  
               ø     Ë      É   y   M      ©             #     &       ¾       ì   ±   _                    |   ã   D   N          ¦   X   n   á   :   ý   Ð   S   î   0       ×   k   +       ´      ³      i       ð   =   ¼             ï   x               Ê   Ù       '   µ   ê                                       (   F   U                     <           ä   9   O   .                º   Ñ           ?        Û      j   /   ]         G             ñ         Â   [                 Z             >                 ¢   	  ¯   u      
%s 
*Clusters are compatible* 
*failure* 
Before running pg_upgrade you must:
  create a new database cluster (using the new version of initdb)
  shutdown the postmaster servicing the old cluster
  shutdown the postmaster servicing the new cluster
 
For example:
  pg_upgrade -d oldCluster/data -D newCluster/data -b oldCluster/bin -B newCluster/bin
or
 
If pg_upgrade fails after this point, you must re-initdb the
new cluster before continuing. 
If you want to start the old cluster, you will need to remove
the ".old" suffix from %s/global/pg_control.old.
Because "link" mode was used, the old cluster cannot be safely
started once the new cluster has been started. 
Performing Upgrade
------------------ 
Report bugs to <%s>.
 
Upgrade Complete
---------------- 
WARNING:  new data directory should not be inside the old data directory, i.e. %s 
WARNING:  user-defined tablespace locations should not be inside the data directory, i.e. %s 
When you run pg_upgrade, you must provide the following information:
  the data directory for the old cluster  (-d DATADIR)
  the data directory for the new cluster  (-D DATADIR)
  the "bin" directory for the old version (-b BINDIR)
  the "bin" directory for the new version (-B BINDIR)
 
Your installation contains extensions that should be updated
with the ALTER EXTENSION command.  The file
    %s
when executed by psql by the database superuser will update
these extensions. 
Your installation contains hash indexes.  These indexes have different
internal formats between your old and new clusters, so they must be
reindexed with the REINDEX command.  After upgrading, you will be given
REINDEX instructions. 
Your installation contains hash indexes.  These indexes have different
internal formats between your old and new clusters, so they must be
reindexed with the REINDEX command.  The file
    %s
when executed by psql by the database superuser will recreate all invalid
indexes; until then, none of these indexes will be used. 
source databases: 
target databases:   $ export PGDATAOLD=oldCluster/data
  $ export PGDATANEW=newCluster/data
  $ export PGBINOLD=oldCluster/bin
  $ export PGBINNEW=newCluster/bin
  $ pg_upgrade
   --clone                       clone instead of copying files to new cluster
   --copy                        copy files to new cluster (default)
   -?, --help                    show this help, then exit
   -B, --new-bindir=BINDIR       new cluster executable directory (default
                                same directory as pg_upgrade)
   -D, --new-datadir=DATADIR     new cluster data directory
   -N, --no-sync                 do not wait for changes to be written safely to disk
   -O, --new-options=OPTIONS     new cluster options to pass to the server
   -P, --new-port=PORT           new cluster port number (default %d)
   -U, --username=NAME           cluster superuser (default "%s")
   -V, --version                 display version information, then exit
   -b, --old-bindir=BINDIR       old cluster executable directory
   -c, --check                   check clusters only, don't change any data
   -d, --old-datadir=DATADIR     old cluster data directory
   -j, --jobs=NUM                number of simultaneous processes or threads to use
   -k, --link                    link instead of copying files to new cluster
   -o, --old-options=OPTIONS     old cluster options to pass to the server
   -p, --old-port=PORT           old cluster port number (default %d)
   -r, --retain                  retain SQL and log files after success
   -s, --socketdir=DIR           socket directory to use (default current dir.)
   -v, --verbose                 enable verbose internal logging
   C:\> set PGDATAOLD=oldCluster/data
  C:\> set PGDATANEW=newCluster/data
  C:\> set PGBINOLD=oldCluster/bin
  C:\> set PGBINNEW=newCluster/bin
  C:\> pg_upgrade
   WAL block size   WAL segment size   block size   checkpoint next XID   data checksum version   dates/times are integers?   first WAL segment after reset   float8 argument passing method   large relation segment size   large-object chunk size   latest checkpoint next MultiXactId   latest checkpoint next MultiXactOffset   latest checkpoint next OID   latest checkpoint oldest MultiXactId   latest checkpoint oldestXID   maximum TOAST chunk size   maximum alignment   maximum identifier length   maximum number of indexed columns   pg_upgrade [OPTION]...

  which is an index on "%s.%s"  which is an index on OID %u  which is the TOAST table for "%s.%s"  which is the TOAST table for OID %u "%s" is not a directory %-*s %d: controldata retrieval problem %d: database cluster state problem %d: pg_resetwal problem %s %s home page: <%s>
 %s() failed: %s %s: cannot be run as root %s: could not find own program executable Adding ".old" suffix to old global/pg_control All non-template0 databases must allow connections, i.e. their
pg_database.datallowconn must be true.  Your installation contains
non-template0 databases with their pg_database.datallowconn set to
false.  Consider allowing connection for all non-template0 databases
or drop the databases which do not allow connections.  A list of
databases with the problem is in the file:
    %s Analyzing all rows in the new cluster Cannot continue without required control information, terminating Cannot upgrade to/from the same system catalog version when
using tablespaces. Checking cluster versions Checking database connection settings Checking database user is the install user Checking for contrib/isn with bigint-passing mismatch Checking for extension updates Checking for hash indexes Checking for incompatible "aclitem" data type in user tables Checking for incompatible "jsonb" data type Checking for incompatible "line" data type Checking for incompatible polymorphic functions Checking for invalid "sql_identifier" user columns Checking for invalid "unknown" user columns Checking for new cluster tablespace directories Checking for prepared transactions Checking for presence of required libraries Checking for reg* data types in user tables Checking for roles starting with "pg_" Checking for system-defined composite types in user tables Checking for tables WITH OIDS Checking for user-defined encoding conversions Checking for user-defined postfix operators Cloning user relation files Consult the last few lines of "%s" for
the probable cause of the failure. Consult the last few lines of "%s" or "%s" for
the probable cause of the failure. Copying old %s to new server Copying user relation files Could not create a script to delete the old cluster's data files
because user-defined tablespaces or the new cluster's data directory
exist in the old cluster directory.  The old cluster's contents must
be deleted manually. Creating dump of database schemas Creating dump of global objects Creating script to delete old cluster Database: %s Deleting files from new %s Failed to match up old and new tables in database "%s" Failure, exiting
 Finding the real data directory for the source cluster Finding the real data directory for the target cluster Freezing all rows in the new cluster In database: %s
 Linking user relation files New cluster data and binary directories are from different major versions. New cluster database "%s" is not empty: found relation "%s.%s" No match found in new cluster for old relation with OID %u in database "%s": %s No match found in old cluster for new relation with OID %u in database "%s": %s Old cluster data and binary directories are from different major versions. Only the install user can be defined in the new cluster. Optimizer statistics are not transferred by pg_upgrade.
Once you start the new server, consider running:
    %s/vacuumdb %s--all --analyze-in-stages Options:
 Performing Consistency Checks
----------------------------- Performing Consistency Checks on Old Live Server
------------------------------------------------ Relation names for OID %u in database "%s" do not match: old name "%s.%s", new name "%s.%s" Resetting WAL archives Restoring database schemas in the new cluster Restoring global objects in the new cluster Running in verbose mode Running this script will delete the old cluster's data files:
    %s SQL command failed
%s
%s Setting frozenxid and minmxid counters in new cluster Setting locale and encoding for new cluster Setting minmxid counter in new cluster Setting next OID for new cluster Setting next multixact ID and offset for new cluster Setting next transaction ID and epoch for new cluster Setting oldest XID for new cluster Setting oldest multixact ID in new cluster Sync data directory to disk The source cluster contains prepared transactions The source cluster lacks cluster state information: The source cluster lacks some required control information: The source cluster was not shut down cleanly. The source cluster was shut down while in recovery mode.  To upgrade, use "rsync" as documented or shut it down as a primary. The target cluster contains prepared transactions The target cluster lacks cluster state information: The target cluster lacks some required control information: The target cluster was not shut down cleanly. The target cluster was shut down while in recovery mode.  To upgrade, use "rsync" as documented or shut it down as a primary. There seems to be a postmaster servicing the new cluster.
Please shutdown that postmaster and try again. There seems to be a postmaster servicing the old cluster.
Please shutdown that postmaster and try again. There were problems executing "%s" This utility can only upgrade from PostgreSQL version %s and later. This utility can only upgrade to PostgreSQL version %s. This utility cannot be used to downgrade to older major PostgreSQL versions. Try "%s --help" for more information.
 Usage:
 When checking a live server, the old and new port numbers must be different. You must have read and write access in the current directory. You must identify the directory where the %s.
Please use the %s command-line option or the %s environment variable. Your installation contains "contrib/isn" functions which rely on the
bigint data type.  Your old and new clusters pass bigint values
differently so this cluster cannot currently be upgraded.  You can
manually dump databases in the old cluster that use "contrib/isn"
facilities, drop them, perform the upgrade, and then restore them.  A
list of the problem functions is in the file:
    %s Your installation contains one of the reg* data types in user tables.
These data types reference system OIDs that are not preserved by
pg_upgrade, so this cluster cannot currently be upgraded.  You can
drop the problem columns and restart the upgrade.
A list of the problem columns is in the file:
    %s Your installation contains roles starting with "pg_".
"pg_" is a reserved prefix for system roles.  The cluster
cannot be upgraded until these roles are renamed.
A list of roles starting with "pg_" is in the file:
    %s Your installation contains system-defined composite types in user tables.
These type OIDs are not stable across PostgreSQL versions,
so this cluster cannot currently be upgraded.  You can
drop the problem columns and restart the upgrade.
A list of the problem columns is in the file:
    %s Your installation contains tables declared WITH OIDS, which is not
supported anymore.  Consider removing the oid column using
    ALTER TABLE ... SET WITHOUT OIDS;
A list of tables with the problem is in the file:
    %s Your installation contains the "aclitem" data type in user tables.
The internal format of "aclitem" changed in PostgreSQL version 16
so this cluster cannot currently be upgraded.  You can drop the
problem columns and restart the upgrade.  A list of the problem
columns is in the file:
    %s Your installation contains the "jsonb" data type in user tables.
The internal format of "jsonb" changed during 9.4 beta so this
cluster cannot currently be upgraded.  You can
drop the problem columns and restart the upgrade.
A list of the problem columns is in the file:
    %s Your installation contains the "line" data type in user tables.
This data type changed its internal and input/output format
between your old and new versions so this
cluster cannot currently be upgraded.  You can
drop the problem columns and restart the upgrade.
A list of the problem columns is in the file:
    %s Your installation contains the "sql_identifier" data type in user tables.
The on-disk format for this data type has changed, so this
cluster cannot currently be upgraded.  You can
drop the problem columns and restart the upgrade.
A list of the problem columns is in the file:
    %s Your installation contains the "unknown" data type in user tables.
This data type is no longer allowed in tables, so this
cluster cannot currently be upgraded.  You can
drop the problem columns and restart the upgrade.
A list of the problem columns is in the file:
    %s Your installation contains user-defined encoding conversions.
The conversion function parameters changed in PostgreSQL version 14
so this cluster cannot currently be upgraded.  You can remove the
encoding conversions in the old cluster and restart the upgrade.
A list of user-defined encoding conversions is in the file:
    %s Your installation contains user-defined objects that refer to internal
polymorphic functions with arguments of type "anyarray" or "anyelement".
These user-defined objects must be dropped before upgrading and restored
afterwards, changing them to refer to the new corresponding functions with
arguments of type "anycompatiblearray" and "anycompatible".
A list of the problematic objects is in the file:
    %s Your installation contains user-defined postfix operators, which are not
supported anymore.  Consider dropping the postfix operators and replacing
them with prefix operators or function calls.
A list of user-defined postfix operators is in the file:
    %s Your installation references loadable libraries that are missing from the
new installation.  You can add these libraries to the new installation,
or remove the functions using them from the old installation.  A list of
problem libraries is in the file:
    %s cannot run pg_upgrade from inside the new cluster data directory on Windows check for "%s" failed: %m check for "%s" failed: %s check for "%s" failed: cannot execute check for "%s" failed: incorrect version: found "%s", expected "%s" child process exited abnormally: status %d child worker exited abnormally: %s cloning "%s" to "%s" command too long copying "%s" to "%s" could not access directory "%s": %m could not add execute permission to file "%s": %s could not clone file between old and new data directories: %s could not connect to source postmaster started with the command:
%s could not connect to target postmaster started with the command:
%s could not create directory "%s": %m could not create file "%s": %s could not create hard link between old and new data directories: %s
In link mode the old and new data directories must be on the same file system. could not create worker process: %s could not create worker thread: %s could not delete directory "%s" could not determine current directory could not determine the number of users could not get control data using %s: %s could not get data directory using %s: %s could not get pg_ctl version data using %s: %s could not get pg_ctl version output from %s could not load library "%s": %s could not open file "%s" for reading: %s could not open file "%s": %s could not open log file "%s": %m could not open version file "%s": %m could not parse version file "%s" could not read line %d from file "%s": %s could not read permissions of directory "%s": %s could not rename file "%s" to "%s": %m could not stat tablespace directory "%s": %s could not write to log file "%s": %m database user "%s" is not the install user directory path for new cluster is too long error while checking for file existence "%s.%s" ("%s" to "%s"): %s error while cloning relation "%s.%s" ("%s" to "%s"): %s error while cloning relation "%s.%s": could not create file "%s": %s error while cloning relation "%s.%s": could not open file "%s": %s error while copying relation "%s.%s" ("%s" to "%s"): %s error while copying relation "%s.%s": could not create file "%s": %s error while copying relation "%s.%s": could not open file "%s": %s error while copying relation "%s.%s": could not read file "%s": %s error while copying relation "%s.%s": could not stat file "%s": %s error while copying relation "%s.%s": could not write file "%s": %s error while copying relation "%s.%s": partial page found in file "%s" error while creating link for relation "%s.%s" ("%s" to "%s"): %s executing: %s fatal file cloning not supported on this platform invalid new port number invalid old port number libpq environment variable %s has a non-local server value: %s linking "%s" to "%s" new cluster binaries reside new cluster data resides new cluster tablespace directory already exists: "%s" notice ok old and new cluster pg_controldata checksum versions do not match old and new pg_controldata WAL block sizes are invalid or do not match old and new pg_controldata WAL segment sizes are invalid or do not match old and new pg_controldata alignments are invalid or do not match.
Likely one cluster is a 32-bit install, the other 64-bit old and new pg_controldata block sizes are invalid or do not match old and new pg_controldata date/time storage types do not match old and new pg_controldata large-object chunk sizes are invalid or do not match old and new pg_controldata maximum TOAST chunk sizes are invalid or do not match old and new pg_controldata maximum identifier lengths are invalid or do not match old and new pg_controldata maximum indexed columns are invalid or do not match old and new pg_controldata maximum relation segment sizes are invalid or do not match old cluster binaries reside old cluster data resides old cluster does not use data checksums but the new one does old cluster uses data checksums but the new one does not old database "%s" not found in the new cluster out of memory pg_ctl failed to start the source server, or connection failed pg_ctl failed to start the target server, or connection failed pg_upgrade upgrades a PostgreSQL cluster to a different major version.

 relname: %s.%s: reloid: %u reltblspace: %s rewriting "%s" to "%s" sockets will be created tablespace directory "%s" does not exist tablespace path "%s" is not a directory template0 must not allow connections, i.e. its pg_database.datallowconn must be false template0 not found too many command-line arguments (first is "%s") user-supplied old port number %hu corrected to %hu warning Project-Id-Version: pg_upgrade (PostgreSQL) 16
Report-Msgid-Bugs-To: pgsql-bugs@lists.postgresql.org
POT-Creation-Date: 2023-08-24 08:49+0000
PO-Revision-Date: 2023-10-01 16:01+0200
Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>
Language-Team: Georgian <nothing>
Language: ka
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.3.2
 
%s 
*áááá¡á¢áá ááá áááá¡áááááá * 
*á¨ááªáááá 
pg_upgrade-áá¡ ááá¨ááááááá áá£áªáááááááá:
  á¨áá¥áááá áá®ááá ááááá¡ áááá¡á¢áá á (initdb-áá¡ áá®ááá ááá á¡ááá)
  ááááá ááá postmaster á¡áá ááá¡á á«ááá áááá¡á¢áá áá
  ááááá ááá postmaster á¡áá ááá¡á áá®áá áááá¡á¢áá áá
 
ááááááááá:
  pg_upgrade -d oldCluster/data -D newCluster/data -b oldCluster/bin -B newCluster/bin
áá
 
áá£ áá á¬áá á¢áááá¡ á¨ááááá pg_upgrade áááá áá£ááá ááá¡á á£ááááá, áááá á«áááááááá
áá®ááá áááá¡á¢áá áá¡ init-db-áá¡ áááááááá á¨ááá«áááá ááááááá ááááá¬ááá. 
áá£ ááááááá, ááá£á¨ááá á«áááá áááá¡á¢áá á, á¡áá­áá áá
á¬áá¨áááá .old á¡á£á¤áá¥á¡á %s/global/pg_control.old-ááá.
áááá¡ áááá, á áá ááááá§ááááá£áá áá§á "ááá£ááá¡" á ááááá, á«áááá áááá¡á¢áá áá¡ ááá¨áááá ááá¡ á¨ááááá, á ááª
áá®ááá á¡áá ááá á áááá¨áá, á«ááááá¡ ááá¨áááá á£á¡áá¤á áá®á áá¦áá áá. 
áááááááá áááá¡ ááááá®áááá
------------------ 
á¨ááªááááááá¡ á¨áá¡áá®áá ááá¬áá áá: %s
 
ááááá®áááá ááá¡á á£áááá£ááá
---------------- 
ááá¤á áá®ááááá:  áá®ááá áááááªáááááá¡ á¡áá¥áá¦áááá á«áááá áááááªáááááá¡ á¡áá¥áá¦ááááá¡ á¨ááááá áá  á£ááá áá§áá¡. ááá: %s 
ááá¤á áá®ááááá:  áááá®ááá ááááá¡ áááá  áá¦á¬áá ááá áªá®á áááá¡ á¡ááá áªááááá¡ áááááá áááááá áááááªáááááá¡ á¡áá¥áá¦ááááá¡ á¨ááááá áá  á£ááá áá§áá¡. ááá: %s 
pg_upgrade-áá¡ ááá¨ááááá¡áá¡ á¡áá­áá áá á¨áááááá ááá¤áá áááªááá¡ áááááªááá:
  á«áááá áááá¡á¢áá áá¡ áááááªáááááá¡ á¡áá¥áá¦áááá  (-d DATADIR)
  áá®ááá áááá¡á¢áá áá¡ áááááªáááááá¡ á¡áá¥áá¦áááá  (-D DATADIR)
  á«áááá ááá á¡ááá¡ "bin" á¡áá¥áá¦áááá (-b BINDIR)
  áá®ááá ááá á¡ááá¡ "bin" á¡áá¥áá¦áááá (-B BINDIR)
 
áá ááá áááá¡ áá¥áááá ááá á¡áá á¨áááªááá¡ ááá¤áá áááááá¡, á ááááááª á£ááá ááááá®áááá¡
ALTER EXTENSION áá á«áááááá.    á¤áááá
    %s
, á ááªá áá¡ á¨áá¡á á£ááááá psql-áá áááááá®ááá ááááá¡ áááá , áááááá®áááá¡ áá
ááá¤áá ááááááá¡. 
áá ááá áááá¡ áá¥áááá ááá á¡áá á°áá¨ ááááá¥á¡ááá¡ á¨áááªááá¡  áá ááááá¥á¡ááá¡ áááá¡á®áááááá£áá
á¨ááá á¤áá ááá¢ááá áááá©áááá á«ááá áá áá®áá áááá¡á¢áá ááá¡ á¨áá áá¡, áá¡á á áá, á¡áá­áá áá áááá
á áááááá¥á¡á REINDEX áá á«áááááá¡ á¡áá¨á£áááááá.  ááááá®ááááá¡ á¨ááááá
REINDEX-áá¡ ááá¡á¢á á£á¥áªáááááª ááááááááªáááá. 
áá ááá áááá¡ áá¥áááá ááá á¡áá á°áá¨ ááááá¥á¡ááá¨ á¨áááªááá¡. áá ááááá¥á¡ááá¡ áá¥áááá¡ á«ááá
áá áá®áá áááá¡á¢áá ááá¨á áááá¡á®áááááá£áá á¨ááá á¤áá ááá¢á áááá©ááá, áá¡á á áá, á¡áá­áá áá áááá
á áááááá¥á¡á REINDEX áá á«áááááá¡ á¡áá¨á£áááááá.  á¤áááá
   %s
, á¨áá¡á á£áááá£áá psql-áá áááááªáááá ááááá¡ áááááá®ááá ááááá¡ áááá , ááááááá á¨áá¥ááá¡ á§áááá
áá áá¡á¬áá  ááááá¥á¡á¡. ááááááá áá, áá ááááá¥á¡áááááá áá áª áá áá ááááá§ááááá£áá áá  áá¥áááá. 
á¡áá¬á§áá¡á áááááá: 
á¡áááááá áááááá:   $ export PGDATAOLD=oldCluster/data
  $ export PGDATANEW=newCluster/data
  $ export PGBINOLD=oldCluster/bin
  $ export PGBINNEW=newCluster/bin
  $ pg_upgrade
   --clone                áá®áá áááá¡á¢áá á¨á á¤ááááááá¡ áááááá ááá ááááá áááá¡ áááªáááá 
   --clone                áá®áá áááá¡á¢áá á¨á á¤ááááááá¡ áááááá ááá ááááá áááá¡ áááªáááá (áááá£ááá¡á®áááá)
   -?, --help                   áá ááá®ááá áááá¡ á©áááááá áá ááá¡ááá
   -B, --new-bindir=BINDIR áá®ááá áááá¡á¢áá áá¡ áááá¨áááá á¤ááááááá¡ á¡áá¥áá¦áááá (áááá£ááá¡á®áááá
                                ááááá á¡áá¥áá¦ááá, á ááª pg_upgrade)
   -D, --new-datadir=DATADIR áá®ááá áááá¡á¢áá áá¡ áááááªáááá á¡áá¥áá¦áááá
   -N, --no-sync             áá  áááááááá áªáááááááááá¡ ááá¡ááá á£á¡áá¤á áá®áá á©áá¬áá áá¡
   -O, -new-options=OPTIONS       á¡áá ááá áá ááááá¡ááªááá áá®ááá áááá¡á¢áá áá¡ ááá áááá¢á ááá
   -P, --new-port=PORT              áá®ááá áááá¡á¢áá áá¡ ááá á¢áá¡ ááááá á (áááá£ááá¡á®áááá %d)
   -U, --username=áááá®ááá ááááá      áááá¡á¢áá áá¡ áááááá®ááá ááááá¡ á¡áá®ááá (áááá£ááá¡á®áááá: "%s")
   -V, --version                 ááá á¡ááá¡ ááá¤áá áááªááá¡ á©áááááá áá ááá¡ááá
   -b, --old-bindir=BINDIR á«áááá áááá¡á¢áá áá¡ áááá¨áááá á¤ááááááá¡ á¡áá¥áá¦áááá
   -c, --check áááá¡á¢áá áááá¡ áá®áááá á¨áááá¬áááá. áááááªááááá áá  á¨áááªááááá
   -d, --old-datadir=DATADIR á«áááá áááá¡á¢áá áá¡ áááááªáááá á¡áá¥áá¦áááá
   -j, --jobs=NUM                áá ááá áá£ááá ááá¡áá¨áááá áá ááªáá¡áááá¡ áá ááááááááá¡ á ááªá®áá
   -k, --link                    áá®áá áááá¡á¢áá á¨á á¤ááááááá¡ ááááááá, ááááá áááá¡ áááªáááá
   -o, -old-options=OPTIONS       á¡áá ááá áá ááááá¡ááªááá á«áááá áááá¡á¢áá áá¡ ááá áááá¢á ááá
   -p, -old-port=PORT             á«áááá áááá¡á¢áá áá¡ ááá á¢áá¡ ááááá á (áááá£ááá¡á®áááá %d)
   -r, --retain                  SQL áá áá£á ááááá¡ á¤áááááá¡ ááá¢ááááá á¬áá ááá¢áááá¡ á¨ááááá 
   -s, --socketdir=DIR á¡áááá¢áá¡ á¡áá¥áá¦áááá (áááá£ááá¡á®ááááá áááááááá á.)
   -v, --verbose                 á¨ááá ááááá¢ááááá áá£á ááááá¡ á©áá ááá
   C:\> set PGDATAOLD=oldCluster/data
  C:\> set PGDATANEW=newCluster/data
  C:\> set PGBINOLD=oldCluster/bin
  C:\> set PGBINNEW=newCluster/bin
  C:\> pg_upgrade
   WAL áááááá¡ áááá   WAL-áá¡ á¡áááááá¢áá¡ áááá  áááááá¡ áááá   á¡ááááá¢á ááá á¬áá á¢áááá¡ á¨áááááá XID   áááááªáááááá¡ á¡ááááá¢á ááá á¯áááá¡ ááá á¡áá   áá áááá áá ááá áá¦ááá ááááá á ááªá®ááááá?   ááá áááá WAL á¡áááááá¢á ááááá¢ááá áááá¡ á¨ááááá   float8 áá áá£áááá¢á áááááá¡ áááááá   áááá á£á áááá ááááá¡ á¡áááááá¢áá¡ áááá   áááá ááááá¥á¢áá¡ áááááá¯áá¡ áááá   á£áá®ááá¡á á¡ááááá¢á ááá á¬áá á¢áááá¡ á¨áááááá MultiXactId   á£áá®ááá¡á á¡ááááá¢á ááá á¬áá á¢áááá¡ á¨áááááá MultiXactOffset   á£áá®ááá¡á á¡ááááá¢á ááá á¬áá á¢áááá¡ á¨áááááá OID   á£áá®ááá¡á á¡ááááá¢á ááá á¬áá á¢áááá¡ á£á«ááááá¡á MultiXactId   á£áá®ááá¡á á¡ááááá¢á ááá á¬áá á¢áááá¡ á£á«ááááá¡á XID   TOAST áááááá¯áá¡ ááá¥á¡ááááá£á á áááá   ááá¥á¡ááááá£á á á¡á¬áá ááá   ááááá¢áá¤áááá¢áá áá¡ ááá¥á¡ááááá£á á á¡ááá á«á   ááááá¥á¡áá ááá£áá á¡ááá¢áááá¡ ááá¥á¡ááááá£á á á áááááááá   pg_upgrade [ááá áááá¢á á]...

  á ááááááª ááááá¥á¡áá "%s.%s"  á ááª áá áá¡ ááááá¥á¡á OID-áá %u  á ááááááª TOAST áªá®á áááá "%s.%s"-á¡áááá¡  á ááááááª TOAST áªá®á áááá %u-á OID-á¡áááá¡ %s á¡áá¥áá¦ááááá¡ áá  á¬áá áááááááá¡ %-*s %d: controldata -áá¡ ááá¦áááá¡ á¨ááªáááá %d: ááááá¡ áááá¡á¢áá áá¡ ááááááá ááááá¡ áá áááááá %d: pg_resetwal -áá¡ áá áááááá %s %s-áá¡ á¡áá¬á§áá¡á áááá ááá: <%s>
 %s()-áá¡ á¨ááªáááá: %s %s: root-áá ááá  ááá£á¨áááá %s: á¡ááá£ááá á áá ááá áááá¡ áááá¨áááá á¤ááááá¡ ááááá á¨áá£á«ááááááá á«ááá ááááááá£á /pg_control-áá ".old" á¡á£á¤áá¥á¡áá¡ ááááá¢ááá á§áááá áá á-template0 áááááá áááá áááá á¨áá¡áá«áááááá á£ááá áá§áá¡, ááá£ áááá
pg_database.datallowconn -á true-á¡ á£ááá á£áááááá¡.  áá¥áááá ááá á¡áá á¨áááªááá¡
áá á-template0 áááááá¡, á ááááááá¡ pg_database.datallowconn -á false-ááá ááá§ááááá£áá.
á¡áá¡á£á ááááá ááá£á¨ááá áááá áááá á§áááá áá á-template0 áááááá, áá
á¬áá¨áááá áááááá, á ááááááááª á¬ááááá áá  áá á¡ááááá¡.
áá áááááá£á á ááááááá¡ á¡ááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
    %s áá®áá áááá¡á¢áá á¨á á§áááá áá¬áá áááá¡ ááááááá ááá  áááá á«áááááá á¡áá­áá á áááá¢á áááá¡ ááá¤áá áááªááá¡ ááá áá¨á, ááá¡á á£áááá áªá®á áááá¡ á¡ááá áªááááá¡ ááááá§áááááá¡áá¡ ááááá á¡áá¡á¢ááá£á á ááá¢áááááá¡ 
ááá á¡ááá¡ áá¥ááááá/áá¥áááááá ááááá®áááá á¨áá£á«ááááááá. áááá¡á¢áá áá¡ ááá á¡ááááá¡ á¨áááá¬áááá áááááªáááá ááááá¡ áááá¨áá áá¡ ááá áááá¢á áááá¡ á¨áááá¬áááá á¨áááá¬áááá, áááá§ááááááá áááá®ááá ááááá ááá§áááááá¡ áááá®ááá ááááá¡ áá£ á£áá áá¡ "contrib/isn"-áá¡ bgint-passing-áá¡ áá -ááááá®áááááá á¨áááá¬áááá ááá¤áá áááááá¡ ááááá®ááááááá¡ á¨áááá¬áááá á°áá¨áá¡ ááááá¥á¡áááá¡ á¨áááá¬áááá áááá®ááá ááááá¡ áªá®á ááááá¨á á¨áá£áááá¡ááááá "aclitem" á¢áááá¡ áááááªáááááá¡ á¨áááá¬áááá á¨áá£áááá¡ááááá "jsonb" áááááªáááá á¢áááá¡ á¨áááá¬áááá á¨áá£áááá¡ááááá "line" áááááªáááá á¢áááá¡ á¨áááá¬áááá á¨áá£áááá¡ááááá ááááááá á¤á£áá á¤á£áá¥áªááááá¡ áá á¡áááááá¡ á¨áááá¬áááá áá áá¡á¬áá á "sql_identifier" áááá®ááá ááááá¡ á¡ááá¢áááá¡ á¨áááá¬áááá áá áá¡á¬áá á "unknown" áááá®ááá ááááá¡ á¡ááá¢áááá¡ á¨áááá¬áááá áá®ááá áááá¡á¢áá áá¡ áªá®á áááááá¡ á¡ááá áªáá¡ á¡áá¥áá¦áááááááá¡ á¨áááá¬áááá ááááááááá£áá á¢á ááááá¥áªááááá¡ á¨áááá¬áááá á¡áá­áá á ááááááááááááá¡ áá á¡áááááá¡ á¨áááá¬áááá áááá®ááá ááááá¡ áªá®á ááááá¨á reg* áááááªáááá¡ á¢áááááá¡ á¨áááá¬áááá á áááááá¡ á¨áááá¬áááá, á áááááááª "pg_"-áá áá¬á§ááá áááá®ááá ááááá¡ áªá®á ááááá¨á á¡áá¡á¢áááá¡ áááá  áááá¡ááá¦áá á£áá áááááááá¢á£á á á¢áááááá¡ á¨áááá¬áááá WITH OIDS áªá®á áááááá¡ á¨áááá¬áááá áááá®ááá ááááá¡ áááá  áááá¡ááá¦áá á£áá ááááá áááá¡ áááááá á¢ááªááá¡ á¨áááá¬áááá áááá®ááá ááááá¡ áááá  áá¦á¬áá ááá postfix áááá áá¢áá áááá¡ áá á¡áááááá¡ á¨áááá¬áááá áááá®ááá ááááá¡ á£á áááá ááááá¡ á¤ááááááá¡ áááááá ááá áááá ááá¡ ááááááá¡ ááááá¡ááááááá ááááááááá ááááá
"%s"-áá¡ áááá á áááááááá á®ááá¡. áááá ááá¡ ááááááá¡ ááááá¡ááááááá ááááááááá ááááá 
"%s"-áá¡ áá "%s"-áá¡ áááá á áááááááá á¤áááá¡. á«áááá %s -áá¡ ááááá ááá áá®áá á¡áá ááá áá áááá®ááá ááááá¡ á£á áááá ááááá¡ á¤ááááááá¡ ááááá ááá áááá®ááá ááááá¡ áááá  áá¦á¬áá ááá áªá®á áááá¡ á¡ááá áªááááá¡ áá áá®ááá áááá¡á¢áá áá¡ 
áááááªáááááá¡ á¡áá¥áá¦ááááá¡ á«áááá áááá¡á¢áá áá¡ á¡áá¥áá¦áááááá¨á áá á¡áááááá¡ áááá
á«áááá áááá¡á¢áá áá¡ áááááªáááááá¡ á¬áá¨ááá¡ á¡áá ááá¢áá¡ á¨áá¥ááá á¨áá£á«ááááááá.  á«áááá áááá¡á¢áá áá¡
 á¨áááªáááááá á®áááá á£ááá á¬áá¨áááá. áááááªáááá ááááá¡ á¡á¥áááááá¡ áááááá¡ á¨áá¥ááá ááááááá£á á ááááá¥á¢áááá¡ áááááá¡ á¨áá¥ááá á«áááá áááá¡á¢áá áá¡ á¬áá¡áá¨ááááá á¡áá ááá¢áá¡ á¨áá¥ááá áááá: %s á¤ááááááá¡ á¬áá¨áá áá®ááá %s-ááá á¨ááªáááá á«ááá áá áá®áá áªá®á ááááá¡ á¨áá áá¡ ááááá®ááááá¡áá¡ ááááá¨á "%s" á¨ááªáááá. ááá¡á á£áááá
 á¡áá¬á§áá¡á áááá¡á¢áá áá¡ á áááá£á á áááááªáááá á¡áá¥áá¦ááááá¡ ááááá á¡áááááá áááá¡á¢áá áá¡ á áááá£á á áááááªáááá á¡áá¥áá¦ááááá¡ ááááá áá®áá áááá¡á¢áá á¨á á§áááá áá¬áá áááá¡ ááá§áááá áááááªáááá ááááá¨á: %s
 áááá®ááá ááááá¡ á£á áááá ááááá¡ á¤ááááááá¡ ááááááá áá®ááá áááá¡á¢áá áá¡ áááááªáááááá¡á áá áááá¨áááá á¤ááááááá¡ á¡áá¥áá¦ááááááá á¡á®ááááá¡á®áá á«áá áááá ááá á¡áááá¡ ááááá£áááááá. áá®ááá áááá¡á¢áá á£áá áááááªáááá áááá "%s" áªáá áááá áá áá: áááááááá á£á áááá áááá "%s.%s" áá®áá áááá¡á¢áá á¨á á«áááá á£á áááá ááááá¡áááá¡ OID -áá %u ááááá¨á "%s" ááááá®áááá ááá  áááááá: %s á«ááá áááá¡á¢áá á¨á áá®ááá á£á áááá ááááá¡áááá¡ OID -áá %u ááááá¨á "%s" ááááá®áááá ááá  áááááá: %s á«áááá áááá¡á¢áá áá¡ áááááªááááá áá áááá¨áááá á¤ááááá¡ á¡áá¥áá¦ááááááá á¡á®ááááá¡á®áá á«áá ááááá ááá á¡ááááááá. áá®ááá áááá¡á¢áá áá¡áááá¡ áá®áááá ááá§áááááá¡ áááá®ááá ááááá¡ ááááááááá á¨áá¡áá«áááááá. ááá¢áááááá¢áá áá¡ á¡á¢áá¢áá¡á¢áá-áá¡ ááááá¢ááá pg_upgrade-áá¡ áááá  áá  á®áááá.
á ááªá áá®áá á¡áá ááá á¡ ááá£á¨áááá, á¨ááá¡á á£ááá áá á«ááááá:
    %s/vacuumdb %s--all --analyze-in-stages ááá áááá ááá:
 áááááááááá á£ááááá¡ á¨áááá¬ááááá¡ á©áá¢áá ááá
----------------------------- áááááááááá á£ááááá¡ á¨áááá¬ááááá¡ á¨áá¡á á£áááá á«ááá áªááªá®áá á¡áá ááá áá
------------------------------------------------ á£á áááá ááááá¡ á¡áá®ááááá OID-áá %u ááááá¨á "%s" áá  áááá®áááá: á«áááá á¡áá®ááá "%s.%s.", áá®ááá áá "%s.%s" WAL áá á¥áááááá¡ ááááá¢ááá ááá áá®áá áááá¡á¢áá á¨á ááááá¡ á¡á¥áááááá¡ áá¦ááááá áá®ááá áááá¡á¢áá á¨á ááááááá£á á ááááá¥á¢áááá¡ áá¦ááááá ááááá¢ááááá á¨áá¢á§áááááááááá¡ á©áá ááá áá á¡áá ááá¢áá¡ ááá¨áááá á«áááá áááá¡á¢áá áá¡ áááááªáááááá¡ á¤áááááá¡ á¬áá¨ááá¡:
   %s SQL áá á«ááááá¡á á¨ááªáááá
%s
%s áá®áá áááá¡á¢áá á¨á frozenxid áá minmxid áááááááááá¡ ááá§ááááá áá®ááá áááá¡á¢áá áá¡ ááááááá¡ áá ááááá áááá¡ ááá§ááááá áá®áá áááá¡á¢áá á¨á minmxid áááááááá¡ ááá§ááááá áá®ááá áááá¡á¢áá áá¡áááá¡ á¨áááááá OID ááá§ááááá áá®ááá áááá¡á¢áá áá¡áááá¡ á¨áááááá áá£áá¢áá¢á ááááá¥áªááá¡ á¬ááááªáááááá¡á áá ID-áá¡ ááá§ááááá áá®ááá áááá¡á¢áá áá¡áááá¡ á¨áááááá á¢á ááááá¥áªááá¡ áááá¥áá¡á áá ID-áá¡ ááá§ááááá á£á«ááááá¡á XID ááá§ááááá áá®ááá áááá¡á¢áá áá¡áááá¡ áá®áá áááá¡á¢áá á¨á á£á«ááááá¡á áá£áá¢áá¢á ááááá¥áªááá¡ ID-áá¡ ááá§ááááá áááááªáááááá¡ á¡áá¥áá¦ááááá¡ ááá¡áááá á¡ááá¥á ááááááªáá á¡áá¬á§áá¡á áááá¡á¢áá á ááááááááá£á á¢á ááááá¥áªáááá¡ á¨áááªááá¡ á¡áá¬á§áá¡ áááá¡á¢áá á¡ ááááááá ááááá¡ ááá¤áá áááªáá áá  áááá©ááá: á¡áá¬á§áá¡ áááá¡á¢áá á¡ áááááá áá á¡áá­áá á áááá¢á áááá¡ ááá¤áá áááªáá áá  áááá©ááá: á¡áá¬á§áá¡á áááá á¬áá¡áááá¡ áááªááá áá  ááááá áá£áá. á¡áá¬á§áá¡á áááá¡á¢áá á áá¦áááááá¡ á ááááá¨á áá§á, á ááªá áááááá¨á.  áááá¡ááá®áááááá áááááá§áááá "rsync", á áááá áª áá¡ áááá£áááá¢ááªááá¨áá, áá ááááá ááá, á áááá áª á«áá ááááá. á¡áááááá áááá¡á¢áá á ááááááááá£á á¢á ááááá¥áªáááá¡ á¨áááªááá¡ á¡áááááá áááá¡á¢áá á¡ ááááááá ááááá¡ ááá¤áá áááªáá áá  áááá©ááá: á¡áááááá áááá¡á¢áá á¡ áááááá áá á¡áá­áá á áááá¢á áááá¡ ááá¤áá áááªáá áá  áááá©ááá: á¡áááááá áááá á¬áá¡áááá¡ áááªááá áá  ááááá áá£áá. á¡áááááá áááá¡á¢áá á áá¦áááááá¡ á ááááá¨á áá§á, á ááªá áááááá¨á.  áááá¡ááá®áááááá, áááááá§áááá "rsync", á áááá áª áá¡ áááá£áááá¢ááªááá¨áá, áá ááááá ááá, á áááá áª á«áá ááááá. á áááá áª á©ááá¡, áá®áá áááá¡á¢áá á¡ postmaster ááá¡áá®á£á ááá.
ááááá ááá postmaster áá ááááááá á¡áªáááá. á áááá áª á©ááá¡, á«ááá áááá¡á¢áá á¡ postmaster á¯áá  ááááá ááá¡áá®á£á ááá.
ááááá ááá postmaster áá ááááááá á¡áªáááá. %s-áá¡ á¨áá¡á á£ááááá¡ áá áááááá áá áá ááá áááá¡ ááááá®áááá PostgreSQL-áá¡ ááá á¡ááá¡ %s-ááá áá ááááá á¨áá£á«ááá. áá áá ááá áááá¡ á¨áá£á«ááá áá®áááá PostgreSQL ááá á¡áááá ááááá¡ááá %s. áá¡ áá ááá ááá áá  á¨ááá«áááá ááááá§ááááá£á áá¥ááá¡ á£á¤á á á«áááá á«áá ááááá PostgreSQL ááá á¡ááááá¡ á©áááá¡áá¬áááá. ááá¢á ááá¤áá áááªááá¡áááá¡ á¡áªáááá '%s --help'.
 ááááá§ááááá:
 áªááªá®ááá á¡áá ááá áá¡ á¨áááá¬ááááá¡áá¡ á«áááá áá áá®ááá ááá á¢áá¡ áááá ááá á¡á®ááááá¡á®áá á£ááá áá§áá¡. áááááááá á á¡áá¥áá¦ááááá¨á á©áá¬áá á/á¬ááááá®ááá¡ á¬ááááááá áá£áªáááááááá. %s-áá¡ á¡áá¥áá¦ááááá¡ ááááá¢áá¤ááááªáá áá£áªáááááááá.
áááááá§áááá áá á«áááááá¡ á¡á¢á áá¥áááá¡ %s ááá áááá¢á á áá ááá áááá¡ áªááááá %s. áá ááá áááá¡ áá¥áááá ááá á¡áá¡á á¨áááªááá¡ "contrib/isn" á¤á£áá¥áªáááá¡, á áááááááª bigint áááááªáááááá¡ á¢ááá¡ áá§á ááááá.  áá¥áááá á«áááá 
áá áá®ááá áááá¡á¢áá ááá bigint áááá¨ááááááááá¡ á¡á®ááááá¡á®áááááá áá ááá£á¨áááááá, áááá¢áá áá¥áááá áááá¡á¢áá áá¡ ááááá®áááá ááááááá 
á¨áá£á«ááááááá.  á¨áááá«áááá, áá¥áááá¡ á«ááá áááá¡á¢áá á¨á, áááááá, á áááááááª "contrib/isn" á¤á£áá¥áªáááá¡ áá§ááááá¡, á¬áá¨áááá
, áááááá®ááá áááá áá áá¦ááááááá á¤á£áá¥áªáááá.  áá áááááá£á á á¤á£áá¥áªááááá¡ á¡áá á¨áááá«áááá áá®áááá á¤áááá¨á:
   %s áá ááá áááá¡ áá¥áááá ááá á¡áá áááá®ááá ááááá¡ áªá®á ááááá¨á reg* áááááªáááááá¡ á¢ááá¡ á¨áááªááá¡
áá áááááªáááááá¡ á¢áááá¡ á¡áá¡á¢ááá£á á OID-ááá pg_upgrade-áá¡ ááá  áá  ááá á©á£ááááá, áááá¢áá áááá¡á¢áá áá¡
ááááá®áááá á¨áá£á«ááááááá.  áá ááááááá¡ ááá¡ááááá ááááá á¨áááá«áááá á¬áá¨áááá á¨áá¡áááááá¡áá¡ á¡ááá¢ááá
áá ááááá®áááá ááááááá ááá£á¨ááá.
áá áááááá£á á á¡ááá¢áááá¡ á¡ááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
   %s áá ááá áááá¡ áá¥áááá ááá á¡áá á¨áááªááá¡ á ááááá¡, á áááááááª
"pg_" á¡á£á¤áá¥á¡áá áá¬á§ááá. áá¡ áá á¡áá¡á¢ááá£á á á áááááá¡áááá¡ á¨áááááá®á£áá á áááááá.
áááá¡á¢áá áá¡ ááááá®áááá á¨áá£á«ááááááá ááááááá, á¡áááá áá á ááááá¡ á¡áá®ááá¡ áá  áááááá á¥áááá.
á áááááá¡ á¡áá, á áááááááª "pg_"-áá áá¬á§ááááá, á¨áááá«áááá áááááá á¤áááá¨á:
    %s áá ááá áááá¡ áá¥áááá ááá á¡áá áááá®ááá ááááá¡ áªá®á ááááá¨á á¡áá¡á¢áááá¡ áááá  áá¦á¬áá áá áááááááá¢á£á  á¢ááááá¡ á¨áááªááá¡.
áá á¢áááá¡ OID-ááá PostgreSQL-áá¡ ááá á¡áááá¡ á¨áá áá¡ áá£ááááá áá áá, áááá¢áá áá áááá¡á¢áá áá¡ ááááá®áááá ááááááá á¨áá£á«ááááááá  á¨áááá«áááá
á¬áá¨áááá áá áááááá£á á á¡ááá¢ááá áá ááááá®áááá ááááááá ááá£á¨ááá.
áá áááááá£á á á¡ááá¢áááá¡ á¡ááá¡ á®áááá á¨áááá«áááá á¤áááá¨á:
   %s áá ááá áááá¡ áá¥áááá ááá á¡áá á¨áááªááá¡ áªá®á ááááá¡, á áááááááª WITH OIDS-áááá
áá¦á¬áá ááá, á ááª áá®áá ááá­áá ááá áá¦áá áá.  á¡áá­áá áá á¬áá¨áááá oid áªá®á ááááá áá á«áááááá
    ALTER TABLE ... SET WITHOUT OIDS;
áªá®á áááááá¡ á¡áá, á ááááá¡ááª áá¡ áá áááááá áááá©áááá, á¨áááá«áááá áá®áááá á¤áááá¨á:
    %s áá ááá áááá¡ áá¥áááá ááá á¡áá ááá®ááá ááááá¡ áªá®á ááááá¨á áááááªáááááá¡ "jsonb" á¢ááá¡ á¨áááªááá¡.
"jsonb"-áá¡ á¨ááá á¤áá ááá¢á á¨áááªáááá 9.4 ááá¢áá¡ áá áá¡, áá¡á, á áá áá áááá¡á¢áá áá¡ ááááá®áááá ááááááá
á¨áá£á«ááááááá  á¨áááá«áááá á¬áá¨áááá áá áááááá£áá á¡ááá¢ááá áá ááááá®áááá ááááááá ááá£á¨ááá.
áá áááááá£á á á¡ááá¢áááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
    %s áá ááá áááá¡ áá¥áááá ááá á¡áá ááá®ááá ááááá¡ áªá®á ááááá¨á áááááªáááááá¡ "jsonb" á¢ááá¡ á¨áááªááá¡.
"jsonb"-áá¡ á¨ááá á¤áá ááá¢á á¨áááªáááá 9.4 ááá¢áá¡ áá áá¡, áá¡á, á áá áá áááá¡á¢áá áá¡ ááááá®áááá ááááááá
á¨áá£á«ááááááá  á¨áááá«áááá á¬áá¨áááá áá áááááá£áá á¡ááá¢ááá áá ááááá®áááá ááááááá ááá£á¨ááá.
áá áááááá£á á á¡ááá¢áááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
    %s áá¥áááá ááááá¢á áááá®ááá ááááá¡ áªá®á ááááá¨á "line" áááááªáááá¡ á¢ááá¡ á¨áááªááá¡.
áá¡ áááááªáááá¡ á¢ááá á¨áááªáááá á¨ááááááá. áá¡ááá á¨ááªáááá ááá¡á á¨áá§áááá/ááááá§ááááá¡ á¤áá ááá¢á. áááá¢áá áá¥áááá
áááá¡á¢áá áá¡ á«áááá ááá á¡ááá¡ ááááá®áááá ááááááá á¨áá£á«ááááááá.  á¨áááá«áááá
á¬áá¨áááá áá¡ á¡ááá¢ááá áá ááááááá ááá£á¨ááá ááááá®áááá.
áá áááááá£áá á¡ááá¢áááá¡ á¡áá á¨áááá«áááá áá®áááá á¤áááá¨á:
   %s áá ááá áááá¡ áá¥áááá ááá á¡áá áááá®ááá ááááá¡ áªá®á ááááá¨á "sql_identifier" áááááªáááááá¡ á¢ááá¡ á¨áááªááá¡.
áááááªáááá¡ áá á¢áááá¡ ááá¡ááá á¨áááá®ááá¡ á¤áá ááá¢á á¨áááªáááá, áá¡á á áá, ááááááá áá áááá¡á¢áá áá¡ ááááá®áááá á¨áá£á«ááááááá.
á¨áááá«áááá á¬áá¨áááá áá áááááá£á á á¡ááá¢ááá áá ááááááá ááá£á¨ááá ááááá®áááá.
áá áááááá£á á áªá®á áááááá¡ á¡ááá¡ á®áááá á¨áááá«áááá á¤áááá¨á:
    %s áá ááá áááá¡ áá¥áááá ááá á¡áá ááá®ááá ááááá¡ áªá®á ááááá¨á áááááªáááááá¡ "unknown" á¢ááá¡ á¨áááªááá¡.
áá¡ áááááªáááá¡ á¢ááá áªá®á ááááá¨á ááá¨áááá£áá áá¦áá áá, áá¡á, á áá áá áááá¡á¢áá áá¡ ááááá®áááá ááááááá
á¨áá£á«ááááááá  á¨áááá«áááá á¬áá¨áááá áá áááááá£áá á¡ááá¢ááá áá ááááá®áááá ááááááá ááá£á¨ááá.
áá áááááá£á á á¡ááá¢áááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
    %s áá ááá áááá¡ áá¥áááá ááá á¡áá ááá®ááá ááááá¡ áááá  áá¦á¬áá áá ááááá§áááááá¡ á¨áááªááá¡.
"jsonb"-áá¡ á¨ááá á¤áá ááá¢á á¨áááªáááá 14-á ááá á¡ááá¨á, áá¡á, á áá áá áááá¡á¢áá áá¡ ááááá®áááá ááááááá
á¨áá£á«ááááááá  á¨áááá«áááá á¬áá¨áááá áá áááááá£áá á¡ááá¢ááá áá ááááá®áááá ááááááá ááá£á¨ááá.
áááá®ááá ááááá¡ áááá  áá¦á¬áá ááá ááááá§ááááááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
    %s áá ááá áááá¡ áá¥áááá ááá á¡áá á¨áááªááá¡ áááá®ááá ááááá¡ áááá  áá¦á¬áá áá ááááá¥á¢ááá¡, á áááááááª
á¨ááá ááááááá á¤á£á á¤á£áá¥áªáááá¡ áá«áá®áááá áá áá£áááá¢áááá¡ á¢áááá "anyarray" áá "anyelement".
áá¡ áááá®ááá ááááá¡ ááá  áá¦á¬áá ááá ááááá¥á¢ááá á£ááá ááááªáááá ááá á¡ááá¡ áá¬áááááá áá
á¨ááááá áá¦ááááááá, á ááá ááá áá®áá á¨áá¡áááááá¡ á¤á£áá¥áªáááá¡ ááááá ááá, áá áá£áááá¢áááá¡ á¢áááá
"anycompatiblearray" áá "anycompatible".
á¤áááá¨á áá á¡ááá£áá áá áááááá£á á ááááá¥á¢ááá:
  %s áá ááá áááá¡ áá¥áááá ááá á¡áá á¨áááªááá¡ áááá®ááá ááááá¡ áááá  áá¦á¬áá áá postfix áááá áá¢áá ááá¡,
á áááááááª áá®áá ááá­áá ááá áá¦áá áá.  á¡áá­áá á áá¥áááá postfix áááá áá¢áá áááá¡ á¬áá¨áá áá áááá 
prefix áááá áá¢áá áááá áá á¤á£áá¥áªááááá¡ ááááá«áá®áááááá á©ááááªááááá.
áááá®ááá ááááá¡ áááá  áá¦á¬áá ááá postfix áááá áá¢áá áááá¡ á¡ááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
   %s áá ááá áááá¡ áá¥áááá ááá á¡áá ááááá áááá¡ á©áá¢ááá áááá áááááááááááá, á áááááááª áá¥áááá¡ á¡áá¡á¢áááá¨á
áá¦ááá©ááááá áá áá.   á¨áááá«áááá áááááá¢áá áá¡ áááááááááááá áá®áá ááá áááá¢á¨á,
áá á¬áá¨áááá á¤á£áá¥áªáááá, á áááááááª ááá áá§áááááá, á«áááá ááá á¡ááááá.
áá áááááá£á á ááááááááááááá¡ á¡ááá¡ ááá®áá á¨áááá«áááá á¤áááá¨á:
    %s pg_upgrade-áá¡ ááá¨áááá Windows-áá áá®ááá áááá¡á¢áá áá¡ áááááªáááááá¡ á¡áá¥áá¦ááááááá á¨áá£á«ááááááá "%s" á¨áááá¬ááááá¡ á¨ááªáááá: %m "%s" á¨áááá¬ááááá¡ á¨ááªáááá: %s "%s" á¨áááá¬ááááá¡ á¨ááªáááá: ááá¨ááááá¡ á¨ááªáááá "%s" á¨áááá¬ááááá¡ á¨ááªáááá: áá áá¡á¬áá á ááá á¡áá: áááááá "%s" áááááááá "%s" á¨áááá áá ááªáá¡á áá áááá áááá£á áá ááá¡á á£ááá á¡á¢áá¢á£á¡áá %d á¨áááá áááá®ááá á áá áááá áááá£á áá ááá¡á á£ááá á¡á¢áá¢á£á¡áá %s áááááá ááá "%s"-ááá "%s"-ááá áá á«ááááá á«ááááá áá á«áááá ááááá ááá "%s"-ááá "%s"-ááá á¡áá¥áá¦ááááá¡ (%s) á¬áááááá¡ á¨ááªáááá: %m á¤ááááá "%s" ááá¨ááááá¡ á¬áááááá¡ ááááá¢áááá¡ á¨ááªáááá: %s áááááªáááááá¡ á«ááá áá áá®áá á¡áá¥áá¦ááááááá¡ á¨áá áá¡ á¤ááááá¡ áááááá áááá¡ á¨ááªáááá: %s ááá  áá£áááá¨áá áááá á¡áá¬á§áá¡ postmaster-á¡, á ááááááª á¨áááááá áá á«áááááá áááá¨áá:
%s ááá  áá£áááá¨áá áááá á¡áááááá postmaster-á¡, á ááááááª á¨áááááá áá á«áááááá áááá¨áá:
%s á¡áá¥áá¦ááááá¡ (%s) á¨áá¥áááá¡ á¨ááªáááá: %m á¤ááááá¡ (%s) á¨áá¥áááá¡ á¨ááªáááá: %s á«áááá áá áá®ááá áááááªáááááá¡ á¡áá¥áá¦ááááááá¡ á¨á£á áá§áá á ááá£ááá¡ á¨áá¥ááá á¨áá£á«ááááááá: %s
ááá£ááá¡ á ááááá¨á á«áááá áá áá®ááá áááááªáááááá¡ á¡áá¥áá¦ááááááá áá áá áá ááááá á¤áááá£á  á¡áá¡á¢ááááá á£ááá áá§áá¡. áááá®ááá á áá ááªáá¡áá¡ á¨áá¥ááá á¨áá£á«ááááááá: %s áááá®ááá á ááááááá¡ á¨áá¥ááá á¨áá£á«ááááááá: %s á¡áá¥áá¦ááááá¡ ("%s") á¬áá¨ááá¡ á¨ááªáááá áááááááá á á¡áá¥áá¦ááááá¡ ááááá¢áá¤ááááªááá¡ áá áááááá ááá  áááááááá áááá®ááá áááááá á áááááááá %s-á¡áááá¡ á¡ááááá¢á ááá áááááªáááááá¡ ááá¦áááá¡ á¨ááªáááá: %s ááá  ááááá¦á áááááªáááá á¡áá¥áá¦áááá %s ááááá§áááááá: %s %s ááááá§áááááá pg_ctl ááá á¡ááá¡ áááááªááááá ááá  ááááá¦á: %s %s ááááá§áááááá pg_ctl ááá á¡áá ááá  ááááá¦á ááááááááááá¡ ("%s") á©áá¢ááá áááá¡ á¨ááªáááá: %s á¨ááªáááá %s-áá¡ á¬áá¡ááááá®áá ááá®á¡ááá¡áá¡: %s á¤ááááá¡ ááá®á¡ááá¡ á¨ááªáááá "%s": %s áá£á ááááá¡ á¤ááááá¡ ááá®á¡ááá¡ á¨ááªáááá "%s": %m ááá á¡ááá¡ á¤ááááá¡ ááá®á¡ááá¡ á¨ááªáááá "%s": %m ááá á¡ááá¡ á¤ááááá¡ áááá£á¨áááááá¡ á¨ááªáááá "%s" á¤ááááááá (%2$s) %1$d-á á®áááá¡ á¬ááááá®áá á¨áá£á«ááááááá: %3$s á¡áá¥áá¦ááááá¡ á¬áááááááá¡ á¬ááááá®áá á¨áá£á«ááááááá "%s": %s ááááá á¥ááááá¡ á¨ááªáááá %s - %s: %m áªá®á áááááá¡ á¡ááá áªáá¡ á¡áá¥áá¦ááááá¡ "%s" áá¦ááá©áááá¡ á¨ááªáááá: %s áá£á ááááá¡ á¤áááá¨á (%s) á©áá¬áá áá¡ á¨ááªáááá: %m áááááªáááá ááááá¡ áááá®ááá ááááá "%s" áá  áá áá¡ ááá§áááááá¡ áááá®ááá ááááá áá®ááá áááá¡á¢áá áá¡ á¡áá¥áá¦ááááá¡ áááááá á«ááááá áá á«áááá á¨ááªáááá á¤ááááá¡ áá á¡áááááá¡ á¨áááá¬ááááá¡áá¡ "%s.%s" ("%s" "%s"): %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) áááááá áááá¡áá¡ ("%s"-ááá "%s"-ááá): %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) áááááá áááá¡áá¡: á¤ááááá¡ ("%s") á¨áá¥áááá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) áááááá áááá¡áá¡: á¤ááááá¡ ("%s") ááá®á¡ááá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááááá áááá¡áá¡ ("%s"-ááá "%s"-ááá): %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááááá áááá¡áá¡: á¤ááááá¡ ("%s") á¨áá¥áááá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááááá áááá¡áá¡ : á¤ááááá¡ ("%s") ááá®á¡ááá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááááá áááá¡áá¡: á¤ááááá¡ ("%s") á¬ááááá®ááá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááááá áááá¡áá¡: á¤ááááá¡ ("%s") áá¦ááá©áááá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááááá áááá¡áá¡: á¤áááá¨á ("%s") á©áá¬áá áá¡ á¨ááªáááá: %s á¨ááªáááá á£á áááá ááááá¡ "%s.%s" ááááá áááá¡áá¡: á¤áááá¨á "%s" áááááááá ááá¬ááááá ááá áááá áá á¨ááªáááá á£á áááá ááááá¡ (%s.%s) ááá£ááá¡ á¨áá¥áááá¡áá¡ ("%s"-ááá "%s"-ááá): %s á¨áá¡á á£áááá: %s á¤áá¢ááá£á á áá áááá¢á¤áá áááá áááááá ááá áá®áá ááá­áá ááá áá áá áá®ááá ááá á¢áá¡ áá áá¡á¬áá á ááááá á áá áá¡á¬áá á á«áááá ááá á¢áá¡ ááááá á libpq-áá¡ ááá áááá¡ áªáááá %s-á¡ áá ááááááá£á á á¡áá ááá áá¡ áááá¨ááááááá: %s áááá©ááá ááá "%s"-ááá "%s"-ááá áá®ááá áááá¡á¢áá áá¡ áááá¨áááá á¤áááááá áááááá áááá¡ áá®ááá áááá¡á¢áá áá¡ áááááªááááá áááááá áááá¡ áá®ááá áááá¡á¢áá áá¡ áªá®á áááááá¡ á¡ááá áªáá¡ á¡áá¥áá¦áááá á£ááá áá á¡ááááá¡: "%s" ááá¤á áá®ááááá áááá® á«áááá áá áá®ááá áááá¡á¢áá áááá¡ pg_controldata -áá¡ á¡ááááá¢á ááá á¯áááá¡ ááá á¡áááá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ WAL-áá¡ áááááá¡ áááááá áá áá¡á¬áá áá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ WAL-áá¡ á¡áááááá¢áá¡ áááááá áá áá¡á¬áá áá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata á¡á¬áá ááááá ááááááá áá áá  áááá®áááá
á¡áááá áá£ááá, áá áá áááá¡á¢áá á 32-ááá¢ááááá, ááááá á©ááá áá 64 á«áááá áá áá®ááá pg_controldata áááááá¡ áááááá ááááááá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ ááá áá¦á/áá ááá¡ á¨áááá®ááá¡ á¢ááááá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ áááá ááááá¥á¢áá¡ áááááá¯áá¡ áááááá áá áá¡á¬áá áá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ TOAST-áá¡ ááá¥á¡ááááá£á á áááááá¯áá¡ áááááá áá áá¡á¬áá áá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ ááááá¢áá¤áááá¢áá áá¡ ááá¥á¡ááááá£á á á¡ááá á«áááá áá áá¡á¬áá áá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata-áá¡ ááá¥á¡ááááá£á á ááááá¥á¡áá ááá£áá á¡ááá¢ááá áá áá¡á¬áá áá áá áá  áááá®áááá á«áááá áá áá®ááá pg_controldata ááá¥á¡ááááá£á á á£á áááá ááááá¡ á¡áááááá¢áá¡ áááááá ááááááá áá áá  áááá®áááá á«áááá áááá¡á¢áá áá¡ áááá¨áááá á¤áááááá áááááá áááá¡ á«áááá áááá¡á¢áá áá¡ áááááªááááá áááááá áááá¡ á«áááá áááá¡á¢áá á áá  áá§ááááá¡ áááááªáááá á¨áááá¬ááááá¡, áááá áá áá®ááá ááááááá¡ á«áááá áááá¡á¢áá á áá§ááááá¡ áááááªáááá á¨áááá¬ááááá¡, áá®ááá áá áá á á«áááá áááááªáááá áááá "%s" áá®áá áááá¡á¢áá á¨á áá  áá áá¡ ááááááá áá áá¡ááááá áá¡á ááá®á¡ááá ááá pg_ctl-áá¡ á¨ááªáááá á¡áá¬á§áá¡á á¡áá ááá áá¡ ááá¨ááááá¡áá¡ áá á¨ááá ááááá¡ á¨ááªáááá pg_ctl-áá¡ á¨ááªáááá á¡áááááá á¡áá ááá áá¡ ááá¨ááááá¡áá¡ áá á¨ááá ááááá¡ á¨ááªáááá pg_upgrade -á PostgreSQL áááá¡á¢áá á¡ á¡á®áá áááááá  ááá á¡áááá áááááá®áááá¡.

 relname: %s.%s: reloid: %u reltblspace: %s ááááá¬áá á "%s"-ááá "%s"-ááá á¡áááá¢ááá á¨ááá¥ááááá áªá®á áááááá¡ á¡ááá áªáá¡ á¡áá¥áá¦áááá "%s" áá  áá á¡ááááá¡ áªá®á áááááá¡ á¡ááá áªáá¡ áááááá %s á¡áá¥áá¦ááááá¡ áá  á¬áá áááááááá¡ template0-ááá áááááá¨áá ááá á¨áá£á«áááááá á£ááá áá§áá¡. ááá£, ááá¡á pg_database.datallowconn ááá áááá¢á á false á£ááá áá§áá¡ template0 ááááááá áá áá ááá¢áá¡ááá¢áá áááá á áá á«áááááá¡-á¡á¢á áá¥áááá¡ áá áá£áááá¢á (ááá ááááá "%s") áááá®ááá ááááá¡ áááá  ááá¬ááááá£áá á«áááá ááá á¢áá¡ ááááá á %hu á¨áá¡á¬áá ááá£ááá %hu ááá¤á áá®ááááá 